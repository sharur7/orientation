# Git Summary

#### What is Git?
*   Git is a free, open source distributed version control system tool designed to handle everything from small to very large projects with speed and efficiency. It was created by Linus Torvalds in 2005 to develop Linux Kernel. Git has the functionality, performance, security and flexibility that most teams and individual developers need.

<img src="https://gitlab.com/sharur7/orientation/-/raw/master/assignments/summary/git.png"><br>

* Version control systems are a category of software tools that helps record changes to files by keeping a track of modifications done to the code.
Git provides with all the Distributed VCS facilities to the user that was mentioned earlier. Git repositories are very easy to find and access.<br>
Eg: 
    * Gitlab
    * SourceForge
    * Google Cloud Source Repositories
    * Apache Allura
    * GitKraken

#### How git workflow works?

There are 4 fundamentals of Git workflow:
1. Workspace - which is our local directory
2. Index- also called stage
3. Local Repository- HEAD
4. Remote Repository

#### What is meant by the term Fork?
A fork is a copy of a repository. Forking a repository allows you to freely experiment with changes without affecting the original project.When you fork a repository, it’s a clone of the entire repository within GitHub with all of its branches. Typically you do a fork if you want to use the current repository as a base to work on a new project or make changes to show differences between the current repository and the new one in large open source projects.

#### What is meant by the term Clone?
Clone is a Git command line utility which is used to target an existing repository and create a clone, or copy of the target repository.When you create a new repository on GitHub, it exists as a remote location where your project is stored. You can clone your repository to create a local copy on your computer so that you can sync between both the local and remote locations of the project.Cloning is ideal for instances when you need a way to quickly get your own copy of a repository where you may not be contributing to the original project.

#### What is meant by the term Merge Request?
Merge requests (aka “MRs”) display a great deal of information about the changes proposed. The body of an MR contains its description, along with its widget (displaying information about CI/CD pipelines, when present), followed by the discussion threads of the people collaborating with that MR.

MRs also contain navigation tabs from which you can see the discussion happening on the thread, the list of commits, the list of pipelines and jobs, the code changes, and inline code reviews.

#### What is meant by term Issues?
The GitLab issue tracker is an advanced tool for collaboratively developing ideas, solving problems, and planning work.

Issues can allow sharing and discussion of proposals before, and during, their implementation between:

You and your team.
Outside collaborators.
They can also be used for a variety of other purposes, customized to your needs and workflow.

#### What is meant by term Branch?
A branch is a version of a project’s working tree. You create a branch for each set of related changes you make. This keeps each set of changes separate from each other, allowing changes to be made in parallel, without affecting each other.

