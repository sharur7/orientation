# Git Lab Summary

#### What is GitLab?
GitLab is a complete DevOps platform, delivered as a single application. This makes GitLab unique and creates a streamlined software workflow, unlocking your organization from the constraints of a pieced together toolchain. Learn how GitLab offers unmatched visibility and higher levels of efficiency in a single application across the DevOps lifecycle.

<img src= "https://gitlab.com/sharur7/orientation/-/raw/master/assignments/summary/gitlab.jpeg">

#### What GitLab does?
GitLab is a single application that spans the entire software development lifecycle. If you're not using GitLab, your DevOps lifecycle is likely spread across any number of applications. These silos take overhead to integrate, manage, configure, and maintain, slowing down your team and your deployments. Moving to a single application will speed up your workflow and help you deliver better software, faster. Learn how GitLab can replace any number of your existing applications.

#### How does GitLab Workflow?
The GitLab Workflow is a logical sequence of possible actions to be taken during the entire lifecycle of the software development process, using GitLab as the platform that hosts your code.

#### Stages in GitLab?
* <b>IDEA:</b> Every new proposal starts with an idea, which usually come up in a chat. For this stage, GitLab integrates with Mattermost.
* <b>ISSUE:</b> The most effective way to discuss an idea is creating an issue for it. Your team and your collaborators can help you to polish and improve it in the issue tracker.
* <b>PLAN:</b> Once the discussion comes to an agreement, it's time to code. But wait! First, we need to prioritize and organize our workflow. For this, we can use the Issue Board.
* <b>CODE:</b> Now we're ready to write our code, once we have everything organized.
* <b>COMMIT:</b> Once we're happy with our draft, we can commit our code to a feature-branch with version control.
* <b>TEST:</b> With GitLab CI, we can run our scripts to build and test our application.
* <b>REVIEW:</b> Once our script works and our tests and builds succeeds, we are ready to get our code reviewed and approved.
* <b>STAGING:</b> Now it's time to deploy our code to a staging environment to check if everything worked as we were expecting or if we still need adjustments.
* <b>PRODUCTION:</b> When we have everything working as it should, it's time to deploy to our production environment!
* <b>FEEDBACK:</b> Now it's time to look back and check what stage of our work needs improvement. We use Cycle Analytics for feedback on the time we spent on key stages of our process.



